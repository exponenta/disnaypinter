﻿using UnityEngine;
using System.Collections;

public class GetTexture : MonoBehaviour {
	
	private Region_Capture monobehaviour;
	private Camera RenderTextureCamera;
	[Space(20)]
	public GameObject Region_Capture;
	[Space(20)]
	public bool FreezeEnable = false;
	public GameObject TogleActivator;
	public Material SaveImageChanal;
	private Texture2D ConvertToTex2D;
	void Start () {
		//SaveImageChanal.mainTexture = Testttt; 
		//TogleActivator = GameObject.Find("FrezeTexture");
		
		
		RenderTextureCamera = Region_Capture.GetComponentInChildren<Camera>();
		
		monobehaviour = Region_Capture.GetComponent<Region_Capture> ();
		
		if (FreezeEnable) {
			monobehaviour.CheckMarkerPosition = true;
		}
		
		StartCoroutine(WaitForTexture());
		
	}
	
	private IEnumerator WaitForTexture() {
		
		yield return new WaitForEndOfFrame ();
		
		if (RenderTextureCamera.targetTexture) {
			GetComponent<Renderer> ().material.SetTexture ("_MainTex", RenderTextureCamera.targetTexture);
			// назначим текстуру на тестовый спрайт:
			//RenderTexture.active = myRenderTexture;
			//ConvertToTex2D.ReadPixels(new Rect(0, 0, RenderTextureCamera.targetTexture.width, RenderTextureCamera.targetTexture.height), 0, 0);
			//ConvertToTex2D.Apply();
			SaveImageChanal.SetTexture ("_MainTex", RenderTextureCamera.targetTexture);
		}
		else StartCoroutine(WaitForTexture());
		
	}
	
	
	
	void LateUpdate () {
		
		if (FreezeEnable && monobehaviour.MarkerIsOUT)
			RenderTextureCamera.enabled = false;
		
		else RenderTextureCamera.enabled = true;
		
		//if (TogleActivator.name == "FrezeTexture") {
		//} else { TogleActivator = GameObject.Find("FrezeTexture"); }
		
		if (TogleActivator.activeSelf == true) {
			RenderTextureCamera.enabled = true;
		} 
		else 
		{
			RenderTextureCamera.enabled = false;
		}
	}
}