﻿using UnityEngine;
using System.Collections;
using System.IO;
public class LoadImgiOS : MonoBehaviour {
	private string portraitDirectory;
	private string PlayerPortraitImage;
	private Texture2D playerPortraitBig;
	private WWW fileReq;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	IEnumerator LoadImageFromPath(){
		portraitDirectory = Path.Combine(Application.persistentDataPath,"Portrait");
		PlayerPortraitImage = Path.Combine(portraitDirectory,"playerPortrait.jpg");
		playerPortraitBig = new Texture2D(400,400);

		if(File.Exists(PlayerPortraitImage))
		{
			yield return StartCoroutine(RequestImage());

			if(fileReq.bytes.Length > 0){
				Debug.Log ("Done Loading image from : " + PlayerPortraitImage);
				fileReq.LoadImageIntoTexture(playerPortraitBig);
				//do anything with the acquired image as texture
			}
			else{
				Debug.Log ("File read failed. the path looked : " + PlayerPortraitImage);
				//GeneralTools.playerPortraitBig = null;
			}
		}
		else{
			Debug.Log ("Folder failed. the path looked : " + PlayerPortraitImage);
		//	GeneralTools.playerPortraitBig = null;
		}
	}

	IEnumerator RequestImage(){
		Debug.Log ("Started Loading image from : " + PlayerPortraitImage);
		fileReq = new WWW("file://" + PlayerPortraitImage);
		while(!fileReq.isDone){
			yield return null;
		}
		yield break;
	}
}
