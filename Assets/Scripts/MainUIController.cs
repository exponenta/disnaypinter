﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUIController : MonoBehaviour {

    public GameObject InstructionContainer;
    public GameObject HelpContainer;
    public RectTransform Target;
    public RectTransform TranslatedPoint;
    public CanvasGroup MainCanvasGroup;
    public float Duration = 1f;

    private float _height;

    private Vector2 _startPosition;
    private bool _toHome = true;

    void Start () {
        _startPosition = TranslatedPoint.anchoredPosition;
    }

    //bool _translate = false;
    Vector3 _targetPos;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_toHome)
            {
                Application.Quit();
            }
            else
            {
                OnHomeClick();
            }
        }
    }
	IEnumerator Move (Vector2 _new)
    {
        if (!_toHome)
            MainCanvasGroup.interactable = false;

        float speed =  (TranslatedPoint.anchoredPosition - _new).magnitude / Duration; 
        while ((TranslatedPoint.anchoredPosition - _new).magnitude > 10f)
        {

            TranslatedPoint.anchoredPosition = Vector2.MoveTowards(TranslatedPoint.anchoredPosition, _new, speed * Time.deltaTime );
            
            yield return new WaitForEndOfFrame();
        }

        TranslatedPoint.anchoredPosition = _new;

        if (_toHome)
            MainCanvasGroup.interactable = true;

    }


    public void TranslateInstruction()
    {
        _toHome = false;
        InstructionContainer.gameObject.SetActive(true);
        HelpContainer.gameObject.SetActive(false);
        StartCoroutine(Move(Target.anchoredPosition));

    }


    public void TranslateHelp()
    {
        _toHome = false;
        InstructionContainer.gameObject.SetActive(false);
        HelpContainer.gameObject.SetActive(true);
        StartCoroutine(Move(Target.anchoredPosition));

    }

    public void OnHomeClick()
    {
        _toHome = true;
        StartCoroutine(Move(_startPosition));
    }

}
