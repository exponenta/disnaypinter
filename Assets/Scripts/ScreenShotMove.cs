﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class ScreenShotMove : MonoBehaviour {

    public float Speed = 100f;
    public RectTransform TargetTransform;
    
    [SerializeField]
    public Canvas _canvas;
    [SerializeField]
    private bool FadeAlpha = true;

    private RawImage _image;
    private RectTransform _transform;
    private RectTransform _canvasTrans;
    private bool _moved = false;

    Vector2 _targetSize;
    Color _color = Color.white;
    float maxMagnitude = 0f;

    void Start () {
        _image = GetComponent<RawImage>();
        _transform = GetComponent<RectTransform>();
        _canvasTrans = _canvas.GetComponent<RectTransform>();
        _targetSize = TargetTransform.sizeDelta;
        gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        if (_moved)
        {
            _transform.sizeDelta = Vector2.Lerp(_transform.sizeDelta, _targetSize, Time.deltaTime * Speed);
            if (Vector2.Distance(_transform.sizeDelta, _targetSize) < 10)
            {
                _moved = false;
                gameObject.SetActive(false);
            }

            if (FadeAlpha)
            {
                _color.a = (_transform.sizeDelta.magnitude - _targetSize.magnitude) / maxMagnitude;
                _image.color = _color;
            }
            
        }
	}

    public void OnStartMove(Texture2D tex)
    {
        _image.texture = tex;
        _image.color = Color.white;
        _transform.sizeDelta = _canvasTrans.sizeDelta* 0.98f;
        maxMagnitude = _transform.sizeDelta.magnitude;
        _moved = true;
        gameObject.SetActive(true);
    }
}
