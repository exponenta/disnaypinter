﻿using UnityEngine;
using System.Collections;

public class TextureSwitcher : MonoBehaviour {

    public RenderTextureCamera RCamera;

    public Renderer TargetRenderer;
    private Material _targetMat;
    [SerializeField]
    private Texture2D _locked;
    [SerializeField]
    private RenderTexture _current;
    private bool started = false;
    void Start() {

        if (RCamera == null)
            RCamera = GameObject.FindGameObjectWithTag("RegionCapture").GetComponent<RenderTextureCamera>();
        if (TargetRenderer == null)
            TargetRenderer = GetComponentInChildren<Renderer>();

        if (RCamera.GetRenderTexture() != null)
        {
            FromInitEvent(RCamera.GetRenderTexture());
        } else
        {
            RCamera.InitComplite += FromInitEvent;
        }
        started = true;

    }

    public void OnEnable()
    {
        if(started)
            FromInitEvent(RCamera.GetRenderTexture());
    }

    public void FromInitEvent(RenderTexture tex)
    {

        _targetMat = TargetRenderer.sharedMaterial;
        _current = tex;
        _targetMat.SetTexture("_MainTex", _current);
        //_targetMat.SetTexture("_Detail", _current);

        RCamera.InitComplite -= FromInitEvent;
    }

    public void LockTexture(bool islock = false)
    {
        if(!islock)
        {
            _targetMat.SetTexture("_MainTex", _current);
            //_targetMat.SetTexture("_Detail", _current);

        }
        else
        {
            if (_locked == null)
                _locked = new Texture2D(_current.width, _current.height);

            RenderTexture.active = _current;
            _locked.ReadPixels(new Rect(0, 0, _current.width, _current.height), 0, 0);
            _locked.Apply();
            //RenderTexture.active = null;

            _targetMat.SetTexture("_MainTex", _locked);
            //_targetMat.SetTexture("_Detail", _locked);

        }
    }
}
