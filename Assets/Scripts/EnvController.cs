﻿using UnityEngine;
using System.Collections;
using System;

public class EnvController : MonoBehaviour {

    public static event Action<Vector2> OnResolutionChange;
    public static event Action<DeviceOrientation> OnOrientationChange;
    public static event Action<ScreenOrientation> OnScreenOrientationChange;
    public static float CheckDelay = 0.5f;        

    static Vector2 resolution;                    
    static DeviceOrientation orientation;        
    static bool isAlive = true;

    public bool ForceOrientation = true;
    public ScreenOrientation Orientation = ScreenOrientation.AutoRotation;
    public bool UsePortrain = false;
    public bool UseInversedPortrain = false;
    public bool UseLandscape = false;
    public bool UseInversedLandscape = false;

    void Start()
    {
        StartCoroutine(CheckForChange());

        SetOrientation(Orientation);
    }

    public void ForceHorizontal()
    {
        SetOrientation(ScreenOrientation.Landscape);
    }

    IEnumerator CheckForChange()
    {
        resolution = new Vector2(Screen.width, Screen.height);
        orientation = Input.deviceOrientation;

        while (isAlive)
        {

            // Check for a Resolution Change
            if (resolution.x != Screen.width || resolution.y != Screen.height)
            {
                resolution = new Vector2(Screen.width, Screen.height);
                if (OnResolutionChange != null) OnResolutionChange(resolution);
            }

            // Check for an Orientation Change
            switch (Input.deviceOrientation)
            {
                case DeviceOrientation.Unknown:            // Ignore
                case DeviceOrientation.FaceUp:            // Ignore
                case DeviceOrientation.FaceDown:        // Ignore
                    break;
                default:
                    if (orientation != Input.deviceOrientation)
                    {
                        orientation = Input.deviceOrientation;
                        if (OnOrientationChange != null) OnOrientationChange(orientation);
                    }
                    break;
            }

            yield return new WaitForSeconds(CheckDelay);
        }
    }

    public void SetOrientation(ScreenOrientation orient)
    {
        if (!ForceOrientation)
            return;

        Screen.autorotateToLandscapeLeft = UseLandscape;
        Screen.autorotateToLandscapeRight = UseInversedLandscape;
        Screen.autorotateToPortrait = UsePortrain;
        Screen.autorotateToPortraitUpsideDown = UseInversedPortrain;


        if (orient == ScreenOrientation.AutoRotation)
        {
            if (Input.deviceOrientation != DeviceOrientation.LandscapeRight)
            {
                Screen.orientation = ScreenOrientation.LandscapeLeft;
            }
            else
            {
                Screen.orientation = ScreenOrientation.LandscapeRight;
            }
        }

        Screen.orientation = orient;
        if (OnScreenOrientationChange != null)
            OnScreenOrientationChange(orient);
    }

    void OnDestroy()
    {
        isAlive = false;
    }

}
