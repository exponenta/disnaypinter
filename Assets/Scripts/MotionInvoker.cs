﻿using UnityEngine;
using System.Collections;

public class MotionInvoker : MonoBehaviour
{

    public bool Detached = false;
  //  private TBPinchToScale _toScale;
  //  private RotateIt _rotateIt;
    private Transform _oldParent;

    private Transform _target;
    private TextureSwitcher _texSwitcher;

    private bool _isTeleporting;
    private bool _isEnableMotion = false;
    
    private Transform _transform;
    private Vector3 _oldScale;
    private TrackableEvent _parentEvent;
    
    void Start()
    {
       // _toScale = GetComponent<TBPinchToScale>();
        //_rotateIt = GetComponent<RotateIt>();
        _oldParent = transform.parent;
        _transform = transform;
        _texSwitcher = GetComponent<TextureSwitcher>();
        _oldScale = _transform.localScale;
        _parentEvent = GetComponentInParent<TrackableEvent>();
    }

    private Vector3 _zero = Vector3.zero;
    private Quaternion _iden = Quaternion.identity;

    private float _Interpoaltor = 0;
    void Update()
    {
        if (_isTeleporting)
        {
            //fast. Need interpolate

            _Interpoaltor += Time.deltaTime * Globals.MotionSpeed; 

            _transform.localPosition = Vector3.Lerp(_transform.localPosition, _zero, _Interpoaltor);

            _transform.localRotation = Quaternion.Slerp(_transform.localRotation, _iden, _Interpoaltor);

            // если возвращаемся назад

            if(_isEnableMotion || _parentEvent.Tracked)
            {
                _transform.localScale = Vector3.Lerp(_transform.localScale,
                        _oldScale, _Interpoaltor); 
            } else {
                _transform.localScale = Vector3.Lerp(_transform.localScale,
                        _zero, _Interpoaltor);
            }

            if (_Interpoaltor > 0.90f)
            {
                _Interpoaltor = 0f;
                _isTeleporting = false;
                _transform.localPosition = _zero;
                _transform.localRotation = _iden;

                _transform.localScale = _oldScale;


                if (!_parentEvent.Tracked && !_isEnableMotion)
                {
                    gameObject.SetActive(false);
                }
                
            }    
        }
    }

    
    void OnEnable()
    {
        GameController.OnMotion += OnMotionEvent;
        _isTeleporting = false;
    }

    void OnDisable()
    {
        _isTeleporting = false;
        GameController.OnMotion -= OnMotionEvent;
        //_rotateIt.enabled = false;
        //_toScale.enabled = false;
    }

    void OnMotionEvent(bool enable)
    {
        if (_isTeleporting && enable)
            return;

        _isTeleporting = true;
        if (enable)
        {
            //_isEnableMotion = true;
            _target = Globals.LockPoint;
            _texSwitcher.LockTexture(true);
            transform.SetParent(_target);
        }
        else
        {
            //if(_parentEvent.Tracked)
                transform.SetParent(_oldParent);
            //_rotateIt.enabled = _toScale.enabled = false;

            //_isEnableMotion = false;
            _texSwitcher.LockTexture();
            _target = _oldParent;
            
        }
        _Interpoaltor = 0;
        _isEnableMotion = enable;
        Detached = enable;
    }
}
