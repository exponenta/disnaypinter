﻿using UnityEngine;
using System.Collections;
using Vuforia;
public class AutoFocus : MonoBehaviour {

    [SerializeField]
	private CameraDevice.FocusMode mFocusMode = CameraDevice.FocusMode.FOCUS_MODE_NORMAL;
    [SerializeField]
    private float Delay = 4f;
    void Start () {

        InvokeFocus();
       //Invoke("InvokeFocus", Delay);
	}

    public void InvokeFocus()
    {
        CameraDevice.Instance.SetFocusMode(mFocusMode);
        Invoke("InvokeFocus", Delay);
    }
}
