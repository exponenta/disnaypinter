﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System;

public class TrackableEvent : MonoBehaviour, ITrackableEventHandler {
    public enum LostMode { Disable, Remove};

    public Region_Capture RCapture;
    public GameObject Prefab;
    public LostMode LostTracking = LostMode.Disable;
    public bool ResetAfterDisabling = true;
    public bool Tracked = false;
        
    private Transform _point;
    private Transform _child;
    //private MotionInvoker _invoker;
    private Vector3 _lastScale;

    void Start () {
        _point = transform.GetChild(0);
        _child = _point.childCount > 0 ? _point.GetChild(0): null;
        if (_child != null)
        {
           // _invoker = _child.GetComponent<MotionInvoker>();
            _lastScale = _child.localScale;
        }
        //if (RCapture == null)
        //    RCapture =  GameObject.FindGameObjectWithTag("RegionCapture").GetComponent<Region_Capture>();
        RCapture = Globals.RegionCapture;

        var mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }


    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
    {
        

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackFound();
            GameController.OnTrackState(true, transform);
            Tracked = true;

        }
        else
        {
            OnTrackLosted();
            GameController.OnTrackState(false, null);
            Tracked = false;
        }
    }

    void OnTrackFound()
    {
        Debug.Log("[TRACKER] FOUND " + name);

        
        if (_child == null)
        {
            _child = (Instantiate(Prefab, _point.position, _point.rotation, _point) as GameObject).transform;
            //_invoker = _child.GetComponent<MotionInvoker>();
            _lastScale = _child.localScale;
        } //else
       // {
           // _child.localScale = _lastScale;
        //}

        if (Globals.DisabledTrackers && Globals._currentTracker != transform)
        {
            _child.gameObject.SetActive(false);
            GameController.OnMotion += OnMotionChnage;
        }
        else
        {
            Globals.RegionCapture.SetImageTarget(this.gameObject);
            _child.gameObject.SetActive(true);
        }
    }

    void OnMotionChnage(bool enable)
    {
        if (!enable)
        {
            Globals.RegionCapture.SetImageTarget(this.gameObject);
            _child.gameObject.SetActive(true);
            GameController.OnMotion -= OnMotionChnage;
        }
    }

    void OnTrackLosted()
    {
        Debug.Log("[TRACKER] LOST " + name);
        if (_child != null && Tracked)
        {
            Globals.RegionCapture.SetImageTarget(null);
            if (Globals.DisabledTrackers)
            {
                return;
            }

            if (LostTracking == LostMode.Disable)
            {
                _child.gameObject.SetActive(false);

                if (ResetAfterDisabling)
                {
                    _child.localPosition = Vector3.zero;
                    _child.localRotation = Quaternion.identity;
                    _child.localScale = _lastScale;
                }
            } else
            {
                _child.gameObject.SetActive(false);
                Destroy(_child.gameObject);
            }
        }
    }
}
