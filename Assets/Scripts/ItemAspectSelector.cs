﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemAspectSelector : MonoBehaviour {
    public bool ControlParentPanel = true;

    private AspectRatioFitter arFilter;
    private LayoutElement leFilter;
    private RawImage image;

	void Start () {
        leFilter = GetComponentInParent<LayoutElement>();
        arFilter = GetComponent<AspectRatioFitter>();
        image = GetComponent<RawImage>();

        Recalculate();   
	}
	
	public void Recalculate () {
        float hw = (float) image.texture.width / image.texture.height;
        arFilter.aspectRatio = hw;
        if(ControlParentPanel)
        {
            leFilter.preferredHeight = image.texture.height;
            leFilter.preferredWidth = image.texture.width;
        }
    }
}
