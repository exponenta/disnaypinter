﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContentScaler : MonoBehaviour
{
    [Tooltip("Цель для скейла")]
    public RectTransform ScaleTarget;
    [Tooltip("ScrolRect компонента, в котором элемент, который скейлиться")]
    public ScrollRect ScrollRect;
    [Tooltip("Для отображения значения скейла")]
    public Text ScaleText;
    [Tooltip("отключение скролла на момент скейла")]
    public bool DisableRectOnScale = true;
    [Tooltip("Скорость скейла")]
    public float ScaleCoof = 0.01f;
    [Tooltip("Минимальное значение для срабатывания скейла ")]
    public float MinDelta = 0.001f;
    [Tooltip("Минимальный скейл")]
    public float MinScale = 0.5f;
    [Tooltip("Максимальный скейл")]
    public float MaxScale = 2.5f;
    [Tooltip("Скейл относительно предыдущего значения")]
    public bool RelativeScale = false;
    public float Scale = 1;

    //   private CanvasScaler _scaler;
    private Vector3 _oldScale;
    private Canvas _canvas;

    void Start()
    {
        //        _scaler = GetComponent<CanvasScaler>();
        _oldScale = ScaleTarget.localScale;
        _canvas = GetComponentInParent<Canvas>();
        if(ScaleTarget == null)
            ScaleTarget = GetComponent<RectTransform>();

    }

    void OnDisable()
    {
        ScaleTarget.localScale = _oldScale;
    }
    void Update()
    {

        if (Input.touchCount > 1)
        {
            Touch t1 = Input.touches[0];
            Touch t2 = Input.touches[1];
            Vector2 avr = (t1.position + t2.position) * 0.5f;
            var delta = Vector2.Distance(t1.position - t1.deltaPosition, t2.position - t2.deltaPosition) - Vector2.Distance(t1.position, t2.position);
           // delta = Input.mouseScrollDelta.magnitude * 10;
            if (Mathf.Abs(delta) >= MinDelta)
            {
                Scale -= delta * ScaleCoof;
                Scale = Mathf.Clamp(Scale, MinScale, MaxScale);

                if (DisableRectOnScale && ScrollRect.enabled)
                {
                    ScrollRect.StopMovement();
                    ScrollRect.enabled = false;
                    //ScrollRect.normalizedPosition += ScrollRect.normalizedPosition * delta * ScaleCoof;

                    /*
                    Vector2 relative;
                    RectTransformUtility.ScreenPointToLocalPointInRectangle(
                        _canvas.transform as RectTransform, avr, null, out relative);

                    if (Scale != MinScale && Scale != MaxScale)
                    {
                        var dp = relative - ScrollRect.content.anchoredPosition;
                        ScrollRect.content.anchoredPosition += dp *(1f - delta * ScaleCoof);
                    }
                    */
                }

                if (RelativeScale)
                    ScaleTarget.localScale = ScaleTarget.localScale * Scale;
                else
                    ScaleTarget.localScale = _oldScale * Scale;

                ScaleText.text = "Scale X" + Scale.ToString("N2");
            }
        }
        else
        {
            if (DisableRectOnScale && !ScrollRect.enabled)
            {
                ScrollRect.enabled = true;
                ScrollRect.normalizedPosition = ScrollRect.normalizedPosition;
            }

        }
    }
}