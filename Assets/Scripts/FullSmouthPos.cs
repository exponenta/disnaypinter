﻿using UnityEngine;

public class FullSmouthPos : MonoBehaviour
{
    public bool _enableMotions = false;
    public bool ScaleAlways = true;
    public GameObject TransformSource;
    public GameObject TransformTarget;

    public float smoothTime = 0.9F;
    private float smoothTimeR = 0.5F;

    private Vector3 velocityPos = Vector3.zero; //  позиция

    private Vector3 velocityScal = Vector3.zero; // размер

    private Vector3 velocityRotXYZ = Vector3.zero; // варащ
    private float velocityRotW = 0; // вращение

    private Vector3 RoXYZ;
    private float RoW;

    public float ScaleSize = 1f;

    private Transform _target;
    private Transform _apparat;

    /*
    private RotateIt _rotatteIt;
    private TBPinchToScale _pinch;
    */
    private bool _autoStop = false;

    private Quaternion _defRotation;
    private Vector3 _defScale;

    void Awake()
    {
        _apparat = TransformTarget.transform;
        _target = TransformSource.transform;
        //_rotatteIt = TransformSource.GetComponent<RotateIt>();
        //_pinch = TransformSource.GetComponent<TBPinchToScale>();

        _defRotation = _target.localRotation;
        _defScale = _target.localScale;

        EnableMotions = _enableMotions;
    }

    void Start()
    {
        GameController.OnMotion += Reset;

    }
    public bool EnableMotions
    {
        set
        {
            _enableMotions = value;
            _autoStop = false;
            //_rotatteIt.enabled = _enableMotions;
            //_pinch.enabled = _enableMotions;
        }
        get
        {
            return _enableMotions;
        }
    }

    private Vector3 targetRotXYZ = Vector3.zero;
    private Quaternion _rotation = Quaternion.identity;
    void Update()
    {

        if (ScaleAlways || _enableMotions)
        {
            //Vector3 targetScale = _target.localScale * ScaleSize; //  плавно
            _apparat.localScale = Vector3.SmoothDamp(_apparat.localScale, _target.localScale * ScaleSize, ref velocityScal, smoothTime);
        }

        if (!_enableMotions)
        {
            return;
        }
        /*
        //Vector3 targetPosition = TargetApparat.transform.position; //  плавно
        _apparat.localPosition = Vector3.SmoothDamp(_apparat.localPosition, _target.localPosition, ref velocityPos, smoothTime);
        */

        //  плавно
        targetRotXYZ.x = _target.localRotation.x;
        targetRotXYZ.y = _target.localRotation.y;
        targetRotXYZ.z = _target.localRotation.z;

        RoXYZ = Vector3.SmoothDamp(RoXYZ, targetRotXYZ, ref velocityRotXYZ, smoothTimeR);

        //Vector3 targetRotBB = new Vector3(TargetApparat.transform.rotation.w, 0, 0);
        RoW = Mathf.SmoothDamp(RoW, _target.localRotation.w, ref velocityRotW, smoothTimeR);
        _rotation.Set(RoXYZ.x, RoXYZ.y, RoXYZ.z, RoW);

        _apparat.localRotation = _rotation;

        if (_autoStop)
        {
            if(/*Vector3.Distance(_apparat.localPosition,_target.localPosition) < 0.01f &&*/
               Mathf.Abs(_target.localScale.sqrMagnitude - _apparat.localScale.sqrMagnitude) < 0.01f &&
               Vector3.Distance(RoXYZ, targetRotXYZ) < 0.01f &&
               Mathf.Abs(RoW - _target.localRotation.w) < 0.01f)
            {
                _enableMotions = false;
                _autoStop = false;
            }

        }
    }

    public void Reset(bool enable)
    {
        EnableMotions = enable;
        _target.localRotation = _apparat.localRotation = _defRotation;
        _target.localScale = _apparat.localScale = _defScale;
        velocityRotW = 0;
        velocityRotXYZ *= 0;
        velocityScal *= 0;
    }
    
    public void AutoStop()
    {
        _autoStop = true;
        _enableMotions = true;
    }
}
