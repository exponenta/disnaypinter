﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Vuforia;

public static class Globals {
    public static Transform LockPoint;
    public static float MotionSpeed = 100f;
    public static bool DisabledTrackers = false;
    public static Region_Capture RegionCapture;
    public static Transform _currentTracker;
}

public class GameController : MonoBehaviour {

    public static event Action<bool> OnAnimation;
    public static event Action<bool> OnMotion;
    public static event Action<bool, Transform> OnTracked;

    public GameObject UI;
    public Toggle DragToogle;
    public CanvasGroup Buttons;
    public Transform LockPoint;
    public float MotionSpeed = 100f;
    public Region_Capture RegionCapture;

    public ScreenShotMove _scrMove;

    public static void OnTrackState(bool status, Transform obj)
    {
        if (OnTracked != null)
            OnTracked(status, obj);
    }

    [SerializeField]
    private bool _thereTrack = false;
    private bool _animationPlayed = false;
    private bool _locked = false;
    
    private Transform _lastActiveObj;
    private ObjectTracker _tracker;

    void OnDestroy()
    {
        OnAnimation = null;
        OnMotion = null;
        OnTracked = null;

        //CaptureAndSaveEventListener.onScreenShotInvoker = null;
        //CaptureAndSaveEventListener.onError -=OnError;
    }

    void Awake()
    {
        Globals.RegionCapture = RegionCapture;

    }

    void Start() {

        ScreenshotManager.ScreenshotFinishedSaving += OnScreenShot;
        //CaptureAndSaveEventListener.onError += OnError;

        DragToogle.onValueChanged.AddListener((x) => { OnMotionChecked(x); });

        OnTracked += OnMarkerTracked;

        Globals.LockPoint = LockPoint;
        Globals.MotionSpeed = MotionSpeed;
        Globals.DisabledTrackers = false;

        _tracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

    }


    public void OnCapture()
    {
        if (_lastActiveObj != null && _thereTrack || _locked)
        {
            string fname = "Object";

            ObjectInfo oi = _lastActiveObj.GetComponent<ObjectInfo>();
            if (oi != null)
                fname = oi.FileName;

            // делаем скрин без UI
            UI.SetActive(false);
            StartCoroutine(ScreenshotManager.Save(fname, "LiveColour"));

            Invoke("OnScreenShot", 0.5f);
            //_captAndSave.FILENAME_PREFIX = fname;
            //_captAndSave.GetFullScreenShot();

        }
    }


    public void OnMotionChecked(bool status)
    {
        _locked = status;

        Globals.DisabledTrackers = _locked;

        if (OnMotion != null && _locked)
            OnMotion(_locked);

        Buttons.interactable = _thereTrack;

        /*
        if (_tracker != null)
        {
            
            if (_locked)
            {
                _tracker.Stop();
            } else
            {
                _tracker.Start();
            }
        }*/
        
        if (OnMotion != null && !_locked)
            OnMotion(_locked);
        
    }

    public void OnError(string msg)
    {
        UI.SetActive(true);
        Debug.LogError(msg);
    }

    public void OnScreenShot()
    {
        Debug.Log("On Screen Shot");
        //_captAndSave.SaveTextureToGallery(screen);
        UI.SetActive(true);
        //_scrMove.OnStartMove(screen);
        
    }

    public void OnAnimationButtonClick()
    {
        if (!_thereTrack&& !_locked)
            return;

        _animationPlayed = !_animationPlayed;
        if (OnAnimation != null)
            OnAnimation(_animationPlayed);
    }

    void OnMarkerTracked(bool tracked, Transform obj)
    {

        _thereTrack = tracked;

        if (tracked)
        {
            Globals._currentTracker = obj;
        }
      //  if (_locked)
        //    return;

        if (!_locked)
        {
            Buttons.interactable = tracked;
            _lastActiveObj = obj;
        }
        if (!tracked)
        {
            _animationPlayed = false;
        }
    }
}
