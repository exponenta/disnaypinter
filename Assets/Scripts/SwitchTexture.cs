﻿using UnityEngine;
using System.Collections;

public class SwitchTexture : MonoBehaviour {
	//   Encompassing_Virtual_Action\Galaxy S6 edge\Phone\Android\data\com.LivePainta.LivePainte\files
	public Renderer _renderer;
    public Vector2 textureSize = new Vector2(32, 32);
	public Material SaveTexMat;
	public Material LoadMat;
	private Texture2D tex;
	private Texture2D TexC;
	// важно сделать подмену материалов а не текстур на модели
	private RenderTexture RendTexTwo;
	public GameObject FrezeTexObj;
	public GameObject ThisObjj;
	private Texture RendTexOne; // та текстура которая по умолчанию включена на животном в начале игры
	public string TextureUniqueName;
	private int SaveAdd; private int LoadAdd; private int SaveInit = 0; private int LoadInit = 0;
	public int WaitTime = 3;

	public Renderer ThisObjRend;
    void Start () {
		//Generate();

        TextureManager.SubFolder = "MainTextures";

		_renderer = GetComponent<MeshRenderer>();   //ThisObjj = GetComponent<GameObject>();
		//ThisObjRend = GetComponent<MeshRenderer>();
    }

	void Update () {
		if (SaveInit == 1)  // все это отложенные действия функций
		{ SaveAdd = SaveAdd + 1;
			if (SaveAdd == 1)
			{ 
				if (ThisObjRend.enabled == true) {
					ThisObjj.GetComponent<SkinnedMeshRenderer> ().material.mainTexture = RendTexOne;
					//ThisObjj.GetComponent<GetTexture>().enabled = true;
					Generate ();
					TextureManager.SaveTexture (TexC, TextureUniqueName + ".png", true);  //testTexture.png

				} // проверка на рендер
				SaveInit = 0;  SaveAdd = 0;
			} // код сейва тут
		

		}
		if (LoadInit == 1) 
		{  LoadAdd = LoadAdd + 1;
			if (LoadAdd == 2)
			{ 
				if (ThisObjRend.enabled == true) {
					RendTexOne = ThisObjj.GetComponent<SkinnedMeshRenderer> ().material.mainTexture;
					//FrezeTexObj.SetActive(false);
					//ThisObjj.GetComponent<GetTexture>().enabled = false;
					tex = TextureManager.LoadTexture (TextureUniqueName + ".png");  //testTexture.png
					if (tex != null) {
						LoadMat.mainTexture = tex;
						ThisObjj.GetComponent<SkinnedMeshRenderer> ().material.mainTexture = tex;
					} 
				} // проверка на рендер
				LoadInit = 0;  LoadAdd = 0;
			} // код лоада тут

		}
	}

    public Texture2D GenerateTexture()
    {

        if(tex == null)
            tex = new Texture2D((int)textureSize.x, (int)textureSize.y);

        Color[] colors = new Color[tex.width * tex.height];

        for (int i=0; i < colors.Length; i++)
        {
            colors[i] =  Random.ColorHSV();
        }

        tex.SetPixels(colors);
        tex.Apply();
        return tex;
    }

    public void Generate()
    {
		
		TexC = new Texture2D( SaveTexMat.mainTexture.width,  SaveTexMat.mainTexture.height);
		RendTexTwo = (RenderTexture)SaveTexMat.mainTexture;
		RenderTexture.active = RendTexTwo;
		TexC.ReadPixels(new Rect(0, 0, SaveTexMat.mainTexture.width, SaveTexMat.mainTexture.height), 0, 0);
		TexC.Apply();


		//Color[] pix = TexC.GetPixels(0, 0, 512, 512);
		//tex = new Texture2D(512, 512);
		//TextureFormat.ARGB32
		//tex = GenerateTexture();
		//tex.SetPixels(pix);
		//tex.Apply();
		LoadMat.mainTexture = TexC;
    }

    public void Save()
	{ SaveInit = 1;
		//FrezeTexObj.SetActive(true);

    }
    
    public void Load()
	{  LoadInit = 1;

    }

    public void Delete()
    {
		TextureManager.DeleteTexture(TextureUniqueName + ".png");  //testTexture.png
    }
}
