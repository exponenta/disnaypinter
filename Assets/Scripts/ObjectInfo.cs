﻿using UnityEngine;
using System.Collections;

public class ObjectInfo : MonoBehaviour {

    public string fileName = "";

    public string FileName {
        get
        {
            if (fileName != "")
                return fileName;
            return name.Split('_')[1];
        }
    }
}
