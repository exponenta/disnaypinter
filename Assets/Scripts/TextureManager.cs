﻿using UnityEngine;
using System.IO;
using System;

public static class TextureManager {

	private static string subFolder = "";

    /// <summary>
    /// Название субдиректории
    /// </summary>
    /// <value>Испльзуется для того, что бы файлы записывались и читались с определенных папок</value>

    public static string SubFolder {
		set {
			if(value != "" ){
				string path = Path.Combine(Application.persistentDataPath, value); 
				DirectoryInfo dirInf = new DirectoryInfo(path);
				if(!dirInf.Exists){
					try{
						dirInf.Create();
					} catch(IOException e) {
						Debug.LogWarning("Can't create folder! \n" + e.Message );
					}
				}
                subFolder = value;
            }
        }
		get {
			return subFolder ;
		}
	}

    private static string FullPath {
        get
        {
            return Path.Combine(Application.persistentDataPath, subFolder);
        }
   }
    /// <summary>
    /// Сохранение текстуры в файл
    /// </summary>
    /// <param name="texture">Текстура для сохранения</param>
    /// <param name="name">Название файла</param>
    /// <param name="force">Форсировать перезапись файла, по умолчанию не форсируется</param>
    /// <returns> Возвращает true если смог сохранить.</returns>
    public static bool SaveTexture(Texture2D texture, string name, bool force=false) {
        if (texture == null)
        {
            Debug.LogError("Texture can'n be null!!");
            return false;
        }

		if(name==""){
			Debug.LogError("Name can't be empty!");
            return false;
		}
		
        string path = Path.Combine(FullPath, name);
        Debug.Log("Output file name: " + path);

        if(File.Exists(path) && !force)
        {
            Debug.LogError("File already exist!! Use force for rewriting file.");
            return false;
        }

        byte[] bytes = texture.EncodeToPNG();

        try
        {
            File.WriteAllBytes(path, bytes);

        } catch(Exception e)
        {
            Debug.LogError("Can't write file. Error: \n" + e.Message);
            return false;
        }

        return true;
	}
	/// <summary>
    /// Чтение текстуры из файла
    /// </summary>
    /// <param name="name">Имя файла</param>
    /// <returns>Возвращает инстанс текстуры или null в случае ошибок.</returns>
	public static Texture2D LoadTexture(string name){

        string path = Path.Combine(FullPath, name);

        if (!File.Exists(path))
        {
            Debug.LogError("File '" + path + "' not exists!");
            return null;
        }

        try
        {
            Texture2D ret = new Texture2D(2, 2, TextureFormat.ARGB32, true);
            byte[] bytes = File.ReadAllBytes(path);
            if (ret.LoadImage(bytes))
            {
                return ret;
            } else
            {
                Debug.LogError("Can't read file.Unknown error!");
                return null;
            }
        }  catch(Exception e)
        {
            Debug.LogError("Can't read file. Error: \n" + e.Message);
            return null;
        }
	}

    /// <summary>
    /// Удаление текстуры
    /// </summary>
    /// <param name="name">Имя удаляемой текстуры</param>
    /// <returns>True если удаление прошло успешно</returns>
    public static bool DeleteTexture(string name)
    {
        string path = Path.Combine(FullPath, name);

        if (!File.Exists(path))
        {
            Debug.LogError("File + '" + path + "' not exists!");
            return false;
        }

        File.Delete(path);
        
        return true;
    }
    
}
