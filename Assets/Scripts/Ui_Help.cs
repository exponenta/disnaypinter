﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class Ui_Help : MonoBehaviour {
 
    public bool CheckBackButton = true;
    public string DefaultScene;
    public GameObject LoadingScreen;
    public GameObject DisabledGameObj;


    void Update()
    {
        if (CheckBackButton)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                GoToScene(DefaultScene);
        }
    }

	public void GoToScene(string name) {

        if (name != "Exit")
        {
            SceneManager.LoadScene(name);
        } else
        {
            Application.Quit();
        }
        
	}

    public void GoToSceneAsynce(string name)
    {
        StartCoroutine(GoToSceneAsynceIEnumerator(name));
    }

    public IEnumerator GoToSceneAsynceIEnumerator(string name) {

        AsyncOperation loading = SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);

        if (DisabledGameObj != null)
            DisabledGameObj.SetActive(false);

        if (LoadingScreen != null)
            LoadingScreen.SetActive(true);
        yield return loading;
        //loading.allowSceneActivation = true;
        if (LoadingScreen != null)
            LoadingScreen.SetActive(false);

    }

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }



}