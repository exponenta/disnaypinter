﻿using System.Collections;
using UnityEngine;

public class RotateIt : MonoBehaviour
{
    [Tooltip("Разница между осями, когда срабатывает поворот")]
    [Range(0,1)]
    public float delta = 0.6f;
    [Tooltip("Количество градусов поворота на экран")]
    public float speed = 360f;
    [Tooltip("Вращать?")]
    public bool isRotate = true;
    public bool Relative = true;

    public Transform RelativeTarget;

    Transform _transform;
    Vector3 direction = Vector3.zero;
    bool toZero = false;
    Vector3 history = Vector3.zero;

    void Start()
    {
        _transform = GetComponent<Transform>();
        if (null == RelativeTarget)
            RelativeTarget = Camera.main.transform;
    }

	// 1 станок 2 вылезает текстбокс сверху когда убран +++
	// 2 стартскрин плохо +++
	// 3 строку межу плиз контакт емайл ниже сделать +++


	// с загр экрана убрать курсив
	// красная надчеркивание
	// емайл ту иос не работает
    Vector3 old_pos;

    float timer = 0;

    void Update()
    {
        if (isRotate)
        {

#if UNITY_EDITOR
            var pos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            if (Input.GetMouseButton(0))
            {
                direction = (pos - old_pos);
                history = (history + direction) * 0.5f;
            }
            else
            {
                direction = Vector3.zero;
                history = Vector3.zero;
            }

            old_pos = pos;
#elif UNITY_ANDROID || UNITY_IOS
            if (Input.touchCount > 0)
            {
                Touch t = Input.touches[0];
                var pos = Camera.main.ScreenToViewportPoint(t.position);
                if (t.phase == TouchPhase.Moved)
                {
                    direction = (pos - old_pos);
                     history = (history + direction)*0.5f;
                }
                else
                {
                    direction = Vector3.zero;
                     history = Vector3.zero; 
                }

                old_pos = pos;
            }
            else
            {
                direction = Vector3.zero;
                history = Vector3.zero;
            }
#endif
            var scr_delta = Vector3.zero;
            var d = Mathf.Abs(history.normalized.x) - Mathf.Abs(history.normalized.y);
            if (Mathf.Abs(d) > delta)
            {
                if (d > 0)
                    scr_delta.x = speed * history.x * Screen.width / Screen.height;
                else
                    scr_delta.y = speed * history.y;
            }
            if (RelativeTarget != null && Relative)
            {
                _transform.RotateAround(_transform.position, RelativeTarget.right, scr_delta.y);
                _transform.RotateAround(_transform.position, RelativeTarget.up, -scr_delta.x);

            }
            else
            {
                _transform.Rotate(scr_delta.y, 0, 0, Space.World);
                _transform.Rotate(0, -scr_delta.x * Mathf.Sign(_transform.up.y), 0, Space.Self);
            }

        }

        else if (toZero)
        {
            _transform.localRotation = Quaternion.RotateTowards(_transform.localRotation, Quaternion.Euler(0, 0, 0), speed * Time.deltaTime);
            if (Mathf.Abs(_transform.eulerAngles.x) <= speed * Time.deltaTime &&
                Mathf.Abs(_transform.eulerAngles.y) <= speed * Time.deltaTime &&
                Mathf.Abs(_transform.eulerAngles.z) <= speed * Time.deltaTime)
            {
                _transform.localRotation = Quaternion.Euler(0, 0, 0);
                toZero = false;
                isRotate = true;
            }
        }

        if (!isRotate && timer > 0)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
                isRotate = true;
        }
    }

    /// <summary>
    /// Красивый поворот в нулевое положение с блокировкой ручного поворота
    /// </summary>
    public void Reset()
    {
        toZero = true;
        isRotate = false;
    }
    
    void OnPinch(PinchGesture gesture)
    {
        isRotate = false;
        timer = 0.3f;
    }
}
