﻿using UnityEngine;
using System.Collections;

public class AnimationInvoker : MonoBehaviour {

    private Animator _animator;

	void Start () {
        _animator = GetComponentInChildren<Animator>();
	}
	
    void OnEnable()
    {
        GameController.OnAnimation += AnimatorState; 
    }

    void OnDisable()
    {
        GameController.OnAnimation -= AnimatorState;
        AnimatorState(false);
    }

    void AnimatorState(bool enable)
    {
        if(_animator != null)
            _animator.SetBool("IsPlay", enable);
    }
    


}
