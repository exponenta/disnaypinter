#pragma warning disable 0168 // variable declared but not used.
#pragma warning disable 0219 // variable assigned but not used.

using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Runtime.InteropServices;

public class ScreenshotManager : MonoBehaviour {
	
	public static event Action ScreenshotFinishedSaving;
	public static event Action ImageFinishedSaving;
    public static event Action ImageCaptured;
#if UNITY_IPHONE
	
	[DllImport("__Internal")]
    private static extern bool saveToGallery( string path );
	
#endif

    public static int MaxInteration = 20;

	public static IEnumerator Save(string fileName, string albumName = "MyScreenshots")
	{
		bool photoSaved = false;
		
		string date = System.DateTime.Now.ToString("dd-MM-yy");
		
		ScreenshotManager.ScreenShotNumber++;
		
		string screenshotFilename = fileName + "_" + ScreenshotManager.ScreenShotNumber + "_" + date + ".png";
		
		Debug.Log("Save screenshot " + screenshotFilename);

#if UNITY_IPHONE
		
			if(Application.platform == RuntimePlatform.IPhonePlayer) 
			{
				Debug.Log("iOS platform detected");
				
				string iosPath = Application.persistentDataPath + "/" + screenshotFilename;
		
				Application.CaptureScreenshot(screenshotFilename);
				
                if (ImageCaptured != null)
                    ImageCaptured.Invoke();
                
                int loop = 0;
				while(!photoSaved && loop < MaxInteration) 
				{
					photoSaved = saveToGallery( iosPath );
					loop++;
                    yield return new WaitForSeconds(.5f);
				}
			
				iPhone.SetNoBackupFlag( iosPath );
			
			} else {
			
				Application.CaptureScreenshot(screenshotFilename);
			
			}
			
#elif UNITY_ANDROID

        if (Application.platform == RuntimePlatform.Android) 
			{
				Debug.Log("Android platform detected");
				
				string androidPath = "/../../../../DCIM/" + albumName + "/" + screenshotFilename;
				string path = Application.persistentDataPath + androidPath;
				string pathonly = Path.GetDirectoryName(path);
				Directory.CreateDirectory(pathonly);
				Application.CaptureScreenshot(androidPath);

                if (ImageCaptured != null)
                    ImageCaptured.Invoke();

                AndroidJavaClass obj = new AndroidJavaClass("com.ryanwebb.androidscreenshot.MainActivity");

                int loop = 0;
				while(!photoSaved && loop < MaxInteration) 
				{
                    loop++;
					photoSaved = obj.CallStatic<bool>("scanMedia", path);

                    yield return new WaitForSeconds(.5f);
				}
		
			} else {
		
				Application.CaptureScreenshot(screenshotFilename);
		
			}
		#else
			
			while(!photoSaved) 
			{
				yield return new WaitForSeconds(.5f);
		
				Debug.Log("Screenshots only available in iOS/Android mode!");
			
				photoSaved = true;
			}
		
		#endif
		
			ScreenshotFinishedSaving();
	}
	
	
	public static IEnumerator SaveExisting(string filePath, bool callback = false)
	{
		bool photoSaved = false;
		
		Debug.Log("Save existing file to gallery " + filePath);

		#if UNITY_IPHONE
		
			if(Application.platform == RuntimePlatform.IPhonePlayer) 
			{
				Debug.Log("iOS platform detected");
				
				while(!photoSaved) 
				{
					photoSaved = saveToGallery( filePath );
					
					yield return new WaitForSeconds(.5f);
				}
			
				iPhone.SetNoBackupFlag( filePath );
			}
			
		#elif UNITY_ANDROID	
				
			if(Application.platform == RuntimePlatform.Android) 
			{
				Debug.Log("Android platform detected");

				AndroidJavaClass obj = new AndroidJavaClass("com.ryanwebb.androidscreenshot.MainActivity");
					
				while(!photoSaved) 
				{
					photoSaved = obj.CallStatic<bool>("scanMedia", filePath);
							
					yield return new WaitForSeconds(.5f);
				}
			
			}
		
		#else
			
			while(!photoSaved) 
			{
				yield return new WaitForSeconds(.5f);
		
				Debug.Log("Save existing file only available in iOS/Android mode!");

				photoSaved = true;
			}
		
		#endif
		
		if(callback)
			ImageFinishedSaving();
	}
	
	
	public static int ScreenShotNumber 
	{
		set { PlayerPrefs.SetInt("screenShotNumber", value); }
	
		get { return PlayerPrefs.GetInt("screenShotNumber"); }
	}
}
